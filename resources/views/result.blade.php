<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Payment</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 80px;
            }
            .title2 {
                text-align:left;
                font-size: 50px;
            }
            .innerTitle{
                margin-left: 50px;
                margin-right: 50px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .paymentErrors{
                text-align:left;
                color: red;
                font-size: 26px;
                margin-left: 15px;
                margin-right: 15px;
                min-width: 100px;
                min-height: 200px;
            }
            .errorHead{
                font-weight:bold;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <div class="title m-b-md" id="success" style="display:none;">
                Success
            </div>
            <div class="title2 m-b-md" id="error" style="display:none;">
                <div class="innerTitle">Sorry! something went wrong.</div>
                <ul class="paymentErrors" id="paymentErrors">
                </ul>
            </div>
        </div>
        <script>
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            const success = urlParams.get('success');
            const error = urlParams.get('error');

            handleResult(success, error);
            
            function handleResult(success, error){
                if(success == "true"){
                    document.getElementById('success').style.display="block";
                    return;
                }

                document.getElementById('paymentErrors').innerHTML+="<p><span class=\"errorHead\">Message</span>: "+urlParams.get('message')+"</p>";
                document.getElementById('error').style.display="block";
            }
        </script>
    </body>
</html>
