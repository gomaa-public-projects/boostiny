<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Payment</title>

        <style>
            .main, .links{
                text-align: center;
                margin:15%;
            }

            .link{
                margin:5%;
            }
        </style>
    </head>
    <body>    
        <div class="main">
            <strong>Total: {{$total}} </strong>
            <p>Please enter your payment info:</p>
            <form>
                <label for="card-number">Card Number</label>
                <input type="text" name="card-number" required digits="16">

                <label for="cvv">CVV</label>
                <input type="text" name="cvv" required digits="3">

                <label for="expiry">Expiry</label>
                <input type="text" name="expiry-year" required digits="2">
                <input type="text" name="expiry-month" required digits="2">
            </form>

            <div class="links">
                <a class="link" href="/paymentCallBack/{{$order_id}}?success=false">Payment Fail</a>
                <a class="link" href="/paymentCallBack/{{$order_id}}?success=true">Payment Success</a>
            </div>
        </div>    
    </body>
</html>
