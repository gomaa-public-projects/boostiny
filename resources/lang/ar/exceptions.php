<?php

return [
    "title" => [
        "default" => "حدث خطأ ما",
        "forbidden" => "بيانات غير صحيحة",
        "profile_not_found" => "لم يتم العثور على حسابك",
        "invalid_credentials" => "بيانات غير صحيحة",
        "technical_exception" => ".يرجى التواصل مع الدعم الفني",
    ],
    
    "message" => [
        "default" => "حدث خطأ ما",
        "token_blacklisted" => "Token is blackListed!",
        "token_invalid" => "Token is invalid!",
        "token_is_missing" => "Token is missing!",
    
        "profile_already_exists" => "الحساب مستخدم بالفعل",
        "not_verified" => "حسابك غير مفعل",
        "invalid_otp" => "رمز التحقق الذي ادخلته غير صحيح",
        "invalid_credentials" => "يرجى التحقق من كلمة المرور الحالية وحاول مرة أخرى",
        "profile_not_found" => "لم نتمكن من العثور على حسابك",
        "forbidden" => "حسابك غير مصرح له الدخول لهذا التطبيق",
        "technical_exception" => "رقم الخطأ: :error",
    ]
];
