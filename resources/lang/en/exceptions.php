<?php

return [
    "title" => [
        "default" => "Something went wrong",
        "forbidden" => "Something went wrong",
        "profile_not_found" => "We couldn't find your account",
        "invalid_credentials" => "Invalid information",
        "technical_exception" => "Please contact technial support.",
    ],
    
    "message" => [
        "default" => "Something went wrong",
        "token_blacklisted" => "Token is blackListed!",
        "token_invalid" => "Token is invalid!",
        "token_is_missing" => "Token is missing!",
        
        "profile_already_exists" => "Account exists already",
        "not_verified" => "Account is not verified yet",
        "profile_not_found" => "Your account was not found",
        "forbidden" => "Your account is not allowed to access this app",
        "technical_exception" => "Error Number: :error.",
    ]
];
