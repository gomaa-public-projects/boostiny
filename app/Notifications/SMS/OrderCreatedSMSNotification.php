<?php

namespace App\Notifications\SMS;

use Illuminate\Support\Facades\App;
use Illuminate\Notifications\Notification;

class OrderCreatedSMSNotification extends Notification
{
    public $product_name, $customer_name, $seller_name;

    public function __construct($seller_name, $product_name, $customer_name)
    {
        $this->seller_name = $seller_name;
        $this->product_name = $product_name;
        $this->customer_name = $customer_name;
    }

    public function via ($notifiable) {
        return [ 'sms' ];
    }

    public function toSms ($notifiable) {

        return [
            'message' => "Hello $this->seller_name, $this->customer_name bought your product: $this->product_name",
        ];
    }
}