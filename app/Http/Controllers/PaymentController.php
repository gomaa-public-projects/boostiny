<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use App\Notifications\Emails\OrderCreatedEmailNotification;
use App\Notifications\SMS\OrderCreatedSMSNotification;

class PaymentController extends Controller
{
    public function payOrder(Order $order)
    {
        $customer = Auth::guard("CustomerGuard")->user(); // Get current customer

        if(!$this->isValidOrder($order) || $order->customer_id != $customer->id)
        return redirect("result?success=false&error=php&message=invalidOrder");

        $order->update(["status" => "payment_in_progress"]);
        return view('payment-info', ["total" => $order->total, "order_id" => $order->id]);
    }

    public function paymentCallBack(Order $order)
    {
        if(!$this->isValidOrder($order))
        return redirect("result?success=false&error=php&message=invalidOrder");

        if(request()->success == "false"){
            $order->update(["status" => "failed"]);
            return redirect("result?success=false&error=php&message=paymentFailed");
        }
        
        $order->update(["status" => "completed"]);

        $this->notifySellers($order);

        return redirect("result?success=true");
    }

    private function isValidOrder($order){
        return $order && in_array($order->status, ["pending", "payment_in_progress"]) && $order->payment_method == "online";
    }

    public function result()
    {
        return view('result');
    }

    private function notifySellers($order){
        foreach($order->products as $product){
            if(!$product->seller) continue;
    
            $product->seller->authUser->notify(new OrderCreatedEmailNotification($product->seller->name, $product->name, $order->customer->name));
            $product->seller->authUser->notify(new OrderCreatedSMSNotification($product->seller->name, $product->name, $order->customer->name));
        }
    }
}
