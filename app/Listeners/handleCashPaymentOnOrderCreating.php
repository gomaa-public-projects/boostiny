<?php

namespace App\Listeners;

class handleCashPaymentOnOrderCreating
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($order)
    {
        if($order->payment_method != "cash") return;

        $order->status = "completed";
    }
}
