<?php

namespace App\Policies;

use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the profile.
     *
     * @param  \App\User  $user
     * @param  profileWith
     * @return mixed
     */
    public function update(?Customer $user, $targetProfile)
    {
        $customer = Auth::guard("CustomerGuard")->user(); // Get current customer

        if(! $customer) {
            return false;
        }

        return $customer->id == $targetProfile->id;
    }
}
