<?php

namespace App\Models;

use Akwad\Guardian\Traits\AuthUserTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasFactory, AuthUserTrait;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'locale',
        'password',
        'auth_user_id'
    ];

    public function getPhoneAttribute(){
        if(!$this->authUser) return;
        return $this->authUser->phone;
    }

    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
