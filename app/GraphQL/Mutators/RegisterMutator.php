<?php

namespace App\GraphQL\Mutators;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Akwad\Guardian\Models\Otp;
use Akwad\Guardian\Models\AuthUser;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Validator;
use Akwad\Guardian\Models\VerificationSession;
use App\MailNotifications\CustomMailNotification;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Exceptions\WrongOldPasswordException;
use Akwad\Guardian\Exceptions\SystemExceptions\GraphQLGeneralException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\ProfileNotFoundException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\InvalidCredentialsException;
use Akwad\Guardian\Exceptions\AuthenticationExceptions\TokenInvalidException;

class RegisterMutator
{
    public function login($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $validator = Validator::make($args, [
            'email' => ['email', 'string'],
            'password' => ['required_with:email','string'],
            'token' => ['required_without:email','string'],
        ], []);

        if ($validator->fails()) {

            Log::channel('authentication')->info("RegisterMutator (login): {$validator->messages()->first()}");

            throw new GraphQLGeneralException(["message" => "{$validator->messages()->first()}"]);
        }

        $user = Auth::guard('AuthUserGuard')->attempt($args);

        Log::channel('authentication')->info("RegisterMutator (login): resolver has finished");
        return $user;
    }

    public function logout($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        Auth::guard('AuthUserGuard')->logout();

        Log::channel('authentication')->info("RegisterMutator (logout): resolver has finished");
        return "User logged out successfully";
    }
}
